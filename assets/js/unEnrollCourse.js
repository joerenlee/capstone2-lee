let params = new URLSearchParams(window.location.search)
let enrollmentsId = params.get('enrollmentsId')
let token = localStorage.getItem('token')
let courseId = localStorage.getItem('unenrolledCourses')


fetch(`https://safe-springs-26725.herokuapp.com/api/users/unenroll/${enrollmentsId}` , {
	method: 'PUT',
	headers: {
		'Authorization': `Bearer ${token}`,
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data => {
	if(data){
		alert('Unenrolled from Course.')
		window.location.replace('./profile.html')
	} else {
		alert('Something went wrong.')
	}
})