let formSubmit = document.querySelector('#createCourse')

formSubmit.addEventListener('submit', (e) => {
	e.preventDefault()

	let name =  document.querySelector('#courseName').value
	let description = document.querySelector('#courseDescription').value
	let price = document.querySelector('#coursePrice').value

	// get JWT from local storage awnd save in a variable called token.

	let token = localStorage.getItem('token')

	fetch('https://safe-springs-26725.herokuapp.com/api/courses' , {
		method: 'POST',
		headers: {
			'Content-type': 'application/json',
			Authorization: `Bearer ${token}`

		},
		body: JSON.stringify({
			name: name, // name(schema): name(variable)
			description: description,
			price: price
		}) 		
	})
	.then(res => res.json())
	.then(data => {
		
		if(data){
			window.location.replace('./courses.html')
		} else {
			alert('Something went wrong. Course not added.')
		}
	})
})