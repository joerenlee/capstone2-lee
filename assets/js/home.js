let navItems = document.querySelector('#navSession');

// localStorage => an object used to store info locally.
let userToken = localStorage.getItem('token');

// localStorage {
// token: "2131451513414151"
// isAdmin: false,
// getItem: function()
// setItem: function()	
// }

// `` = backsticks/template literals

// conditional rendering - add or delete an html element based on a condition
if(!userToken) { // if user is a guest
	navItems.innerHTML += 
	`
	<li class="nav-item">
		<a href="./pages/login.html" class="nav-link"> Log in </a>
	</li>
	<li class="nav-item">
		<a href="./pages/register.html" class="nav-link"> Register </a>
	</li>
	
	`
	
} else {

	navItems.innerHTML += 
	`
	<li class="nav-item">
		<a href="./pages/profile.html" class="nav-link"> Profile </a>
	</li>
	<li class="nav-item">
		<a href="./pages/logout.html" class="nav-link"> Log Out</a>
	</li>
	
	`

}

// getItem(nameOfKey)

let isAdmin = localStorage.getItem('isAdmin') // items from local storage are strings.
if(!userToken) {
	userGreet.innerHTML += `Welcome, Guest!`
} else {
	if(isAdmin === 'true') {
		userGreet.innerHTML += `Welcome, Admin!`
	} else {
		userGreet.innerHTML += `Welcome, User!`
	}
}

$(".navbar-toggler-icon").click(function () {
    $(this).toggleClass("down");
})

