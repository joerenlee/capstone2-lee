let fullName = document.querySelector('#fullName')
let mobileNo = document.querySelector('#mobileNo')
let isAdmin = localStorage.getItem('isAdmin') 
let token = localStorage.getItem('token')
let container = document.querySelector('#profileContainer')


fetch('https://safe-springs-26725.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Authorization': `Bearer ${token}`,
		'Content-type': 'application/json'
	}
})
.then(res => res.json())
.then(data => {
	if(isAdmin == 'false'){
		fullName.innerHTML = data.firstName + ' ' + data.lastName
		mobileNo.innerHTML += 'Mobile Number: ' +  data.mobileNo
			let coursesEnrolled;
		if(data.enrollments < 1) {
			coursesEnrolled = 'No Courses Enrolled In'
		} else {
			data.enrollments.map(enrollments => {
				container.innerHTML +=
				`<div class="card text-center col-md-6">
				  	<div class="card-body">
				    	<h5 class="courseName">${enrollments.courseName}</h5>
				    	<p class="dateEnrolled">Date Enrolled: ${enrollments.enrolledOn}</p>
				    	<p class="dateEnrolled">Status: ${enrollments.status}</p>
				    	<a href= "./unenrollCourse.html?enrollmentsId=${enrollments._id}" class="btn btn-danger text-white btn-block" id="unEnroll">Unenroll</a>
				  	</div>
				 </div>`
				 let unEnroll = document.querySelector('#unEnroll')
				 unEnroll.addEventListener('click', (e) =>{
					localStorage.setItem('unenrolledCourses', enrollments.courseId)
				})
			})
		}
	} else {
		alert('No Profile will be displayed')
	}
})



