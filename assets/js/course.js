// project the details of our selected course into that jumbotron
// to get the details of our selected course, we have to pass the id of the course in our fetch url.
// We have to get the value/parameter that is set into our url

// window.location.search - returns query string part of the url

// below is to get the value of courseId from the query string.
let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')
let adminUser = localStorage.getItem('isAdmin') 
// populate the info of a course. But first, we have to gain access and save our elements into variables, so that we can project our course details into those elements.

let courseName = document.querySelector('#courseName')
let courseDesc = document.querySelector('#courseDesc')
let coursePrice = document.querySelector('#coursePrice')
let enrollContainer = document.querySelector('#enrollContainer')

// get our course details
// fetch runs immediately
fetch(`https://safe-springs-26725.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	if(adminUser !== 'true' && token !== null) {
		courseName.innerHTML = data.name // can use template literal
		courseDesc.innerHTML = data.description 
		coursePrice.innerHTML = data.price

		
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
		// add a click event to button
		document.querySelector('#enrollButton').addEventListener('click', () => { 
			// no need to add e.preventDefault bc the click event doesn't have a default behavior that refreshes page unlike submit
			fetch('https://safe-springs-26725.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-type':'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId,
					courseName: data.name
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data){
					alert('You have enrolled successfully.')
					window.location.replace('./courses.html')
					} else {
					alert('Enrollment Failed.')
				}
			})
		})
	} else if (adminUser == 'true'){
		courseName.innerHTML = data.name // can use template literal
		courseDesc.innerHTML = data.description 
		coursePrice.innerHTML = data.price

		let studentsEnrolled;
		if(data.enrollees < 1) {
		studentsEnrolled = 'No Students Currently Enrolled'
		}
		
		studentsEnrolled = data.enrollees.map(enrollees => {
			return (
			`
			<div class="card text-center">
  				<div class="card-body col-md-6 offset-md-3">
    				<h5 class="userName">${enrollees.firstName} ${enrollees.lastName}</h5>
    				<p class="dateEnrolled">Date Enrolled: ${enrollees.enrolledOn}</p>
  				</div>
  			</div>
  			`
  			)
  		}).join("")
  		let container = document.querySelector('#enrollContainer')
		container.innerHTML = studentsEnrolled
	} else {
		courseName.innerHTML = data.name // can use template literal
		courseDesc.innerHTML = data.description 
		coursePrice.innerHTML = data.price

		
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
		// add a click event to button
		document.querySelector('#enrollButton').addEventListener('click', () => {
			window.location.replace('../pages/register.html')
		})
	}
})

