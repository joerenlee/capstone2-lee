//We use URLSearchParams to access specific parts of query strings in the URL

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

// this fetch will be run automatically to the deleteCourse page
fetch(`http://localhost:4000/api/courses/deactivate/${courseId}`, {
	method: 'PUT',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data){
		alert('Course Archived.')
	} else {
		alert('Something went wrong.')
	}
})