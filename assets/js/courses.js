
let adminUser = localStorage.getItem('isAdmin') 
let addButton = document.querySelector('#adminButton') // will allow us to add a button to each of our courses to redirect to the specific course.
let cardFooter;
let token = localStorage.getItem('token')


if(adminUser == 'false' || !adminUser) {
	
} else {
	addButton.innerHTML += `
	<div class = 'col-md-6 offset-md-3'>
		<a href= '../pages/addCourse.html' class='btn btn-primary btn-block'>Add Course</a>
	</div>
	`
}
fetch('https://safe-springs-26725.herokuapp.com/api/courses/allcourses', {
	method: 'GET',
	headers: {
	'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
let courseData;
	if(data.length < 1) {
		courseData = 'No Courses Available'
	} else if (adminUser == 'true') {
		courseData = data.map(course => { 
				cardFooter = `<a href= "./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`

				if(course['isActive']) {
					cardFooter += `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger btn-block">Archive Course</a>`
				} else {
					cardFooter += `<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success btn-block" >Activate Course</a>`
				}
			// return each item in the new array. 

			return (
				`
				<div class="col-md-6 my-3">
					<div class = "card">
						<div>
						<img src="../assets/images/card.jpeg" class="card-img-top">
						</div>
						<div class="card-body">
							<h5 class = "card-title"> ${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-right">₱ ${course.price}</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
				`


				)
		}).join("")
	}
	else {
		// Get all Active Courses request
fetch('https://safe-springs-26725.herokuapp.com/api/courses/')
.then(res => res.json())
.then(data => {
	let courseData;

	// to check if there are any active courses
	if(data.length < 1) {
		courseData = 'No Courses Available'
	} else {
		courseData = data.map(course => { 
			if(adminUser == 'false' || !adminUser){
				// ? - URL Parameter
				cardFooter = `<a href= "./course.html?courseId=${course._id}" class="btn btn-dark text-white btn-block">Go To Course</a>`
			} else { // if user is Admin
				cardFooter = `<a href= "./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block" id="singleCourse">Go To Course</a>`
				cardFooter += `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger btn-block">Archive Course</a>`
		}
			// return each item in the new array. 
			return (
				`
				<div class="col-md-6 my-3">
					<div class = "card">
						<div>
						<img src="../assets/images/card.jpeg" class="card-img-top">
						</div>
						<div class="card-body">
							<h5 class = "card-title"> ${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-right">₱ ${course.price}</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
				`

				)
		}).join("")
		// join() - joins all array elements into a string. The argument passed becomes the separator for each item. Because each item is separated by a comma.
	}
	let container = document.querySelector('#coursesContainer')

	container.innerHTML = courseData

})
	}
	let container = document.querySelector('#coursesContainer')

	container.innerHTML = courseData

})