//We use URLSearchParams to access specific parts of query strings in the URL

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

// this fetch will be run automatically to the deleteCourse page
fetch(`https://safe-springs-26725.herokuapp.com/api/courses/activate/${courseId}`, {
	method: 'PUT',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data){
		alert('Course Activated.')
	} else {
		alert('Something went wrong.')
	}
})