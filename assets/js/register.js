let registerForm = document.querySelector('#registerForm')

registerForm.addEventListener('submit', (e) => { // (e) - event object
	//call event
	e.preventDefault(); // prevents register form from resetting once register button or submit is clicked

	let firstName = document.querySelector('#firstName').value;
	let lastName = document.querySelector('#lastName').value;
	let mobileNumber = document.querySelector('#mobileNumber').value;
	let userEmail = document.querySelector('#userEmail').value;
	let password1 = document.querySelector('#password1').value;
	let password2 = document.querySelector('#password2').value;
	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)) {
		// fetch(url, object params) function - get info from backend
		// fetch is the 
		fetch('https://safe-springs-26725.herokuapp.com/api/users/email-exists', {
			method: 'POST', // from route http method
			headers: { // headers are infos not seen by users. contains Content-type and auth.
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ // infos to be seen by user. (req.body)
				email: userEmail
				// email
			})
		})
		.then(res => res.json()) // converts the response to json
		.then(data => { // data is the response now in a json format. true/false. if true, email exists. if false, otherwise.
			if(data === false) {
				// nest a fetch request using the registration to register our user.
				fetch('https://safe-springs-26725.herokuapp.com/api/users/', {
					method: 'POST', 
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: userEmail,
						password: password1,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {
				// true if success registration.
					if(data === true) {
						alert('Registration Successful')
						window.location.replace("./login.html") // replace the current document with the provided method. after registration, user will be redirected to login page.
					} else {
						alert('Registration Failed')
					}
				})
			} else {
				alert ("Email Already Exists.")
			}
		})
	} else {
		alert ("Invalid Input.")	
	}
})

