let loginForm = document.querySelector('#logInUser')

//add submit event to login from.


loginForm.addEventListener("submit" , (e) => {

	e.preventDefault();

	let email = document.querySelector('#userEmail').value
	let password = document.querySelector('#password').value

	if(email == "" || password == ""){
		alert('Please input your email/password.')
	} else {
		fetch('https://safe-springs-26725.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
				if (data !== false && data.accessToken !== null) {
					//if able to get a token,  then use it to get user's details
					// store the token in the localStorage
					// setItem(nameOfKey, value)
					localStorage.setItem('token', data.accessToken)
					fetch('https://safe-springs-26725.herokuapp.com/api/users/details', {
						headers: {
							Authorization: `Bearer ${data.accessToken}`
						}

					})
					.then(res => res.json())
					.then(data => {
						localStorage.setItem('id', data._id)				
						localStorage.setItem('isAdmin', data.isAdmin)					
						localStorage.setItem('courses', JSON.stringify(data.enrollments))	
						window.location.replace('../index.html')				
					})
				} else {
					alert('Login Failed. Something went wrong.')
				}

		})
	}
})