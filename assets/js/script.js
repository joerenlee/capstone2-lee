let navItems = document.querySelector('#navSession');

// localStorage => an object used to store info locally/ storage within a browser.
let userToken = localStorage.getItem('token');

// localStorage {
// token: "2131451513414151"
// isAdmin: false,
// getItem: function()
// setItem: function()	
// }

// `` = backsticks/template literals

// conditional rendering - add or delete an html element based on a condition
if(!userToken) { // if user is a guest
	navItems.innerHTML += 
	`
	<li class="nav-item">
		<a href="./login.html" class="nav-link"> Log in </a>
	</li>
	<li class="nav-item">
		<a href="./register.html" class="nav-link"> Register </a>
	</li>
	`
} else {

	navItems.innerHTML += 
	`
	<li class="nav-item active">
		<a href="./profile.html" class="nav-link"> Profile </a>
	</li>
	<li class="nav-item">
		<a href="./logout.html" class="nav-link"> Log Out</a>
	</li>
	
	`

}

$(".navbar-toggler-icon").click(function () {
    $(this).toggleClass("down");
})